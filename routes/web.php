<?php

use App\Http\Controllers\CategorieController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProduitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('accueil');
Route::get('/',[HomeController::class,"index"])->name('home.index');
Route::get('/categories/recherche', [CategorieController::class, 'recherche'])->name('categories.recherche');
Route::get('/',[HomeController::class,"index"])->name('home.index');
// Route::post('/commandes/client/{id}',[CommandeController::class,"store"])->name('commande.store_client');
Route::post('/commandes/client/{id}',[CommandeController::class,"store"])->name('commande.store_client');
// Route::match(['get', 'post'], '/commandes/client/{id}', [CommandeController::class, "store"])->name('commande.store_client');

Route::get('/panier',[HomeController::class,"show_panier"])->name('home.panier');
Route::get('/panier/clear',[HomeController::class,"clear"])->name('home.clear');
Route::post('panier/add/{id}',[HomeController::class,"add"])->name('home.add');
Route::delete('panier/delete/{id}',[HomeController::class,"delete"])->name('home.delete');
Route::resource("categories",CategorieController::class);
Route::resource("produits",ProduitController::class);
Route::resource("clients",ClientController::class);
Route::resource("commandes",CommandeController::class);


// Route::resource("categories",CategorieController::class);
// Route::resource("produits",ProduitController::class);
// Route::resource("clients",ClientController::class);
// Route::resource("commandes",CommandeController::class);