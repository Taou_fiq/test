<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Models\Categorie;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $designation = $request->query('designation');
        $min_prix = $request->query('min_prix');
        $max_prix = $request->query('max_prix');
        $qnt_null = $request->query('qnt_null');
        $query_builder = Produit::query();
        if ($designation) {
            $query_builder->where('designation', 'like',    '%' . $designation . '%');
        }

        if ($qnt_null) {
            $query_builder->where('quantite_stock', '=', 0);
        }

        if ($min_prix && $max_prix) {
            $query_builder->whereBetween('prix_u', [$min_prix, $max_prix]);
        }
        $nbr_produit = count($query_builder->get());
        $produits = $query_builder->paginate(10)->appends(request()->query());


        return view("produits.index",compact('produits','designation','min_prix','max_prix','qnt_null','nbr_produit'));



    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories=Categorie::all();
        return view('produits.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validateData = $request->validate([
            'designation'=>'required|unique:produits,designation',
            'prix_u'=>'required|integer|between:0,99999',
            'quantite_stock'=>'required|integer|between:1,9999',
            'categorie_id'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->hasFile('image')){
            $imagePath = $request->file('image')->store('produits/images','public');
            $validateData['image'] = $imagePath;
        }
        // $validateData['image'] = $request->file('image')->store('produits/images','public');
        Produit::create($validateData);
        return  redirect()->route('produits.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $pro=Produit::find($id);

        return view('produits.show')->with("pro",$pro);
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $categories=Categorie::all();
        $pro=Produit::find($id);
        return view('produits.edit',compact('pro','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'designation'=>'required|unique:produits,designation',
            'prix_u'=>'required|integer|between:0,99999',
            'quantite_stock'=>'required|integer|between:1,9999',
            'categorie_id'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $pro=Produit::find($id);
        $pro->update($request->all());
        return redirect()->route('produits.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Produit::destroy($id);
        return  redirect()->route('produits.index');

        //
    }
}
