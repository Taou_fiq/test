@extends('layouts.admin')
 @section('title','Modifier une produit')
 @section('content')
    <h1>Modifier la produit N° {{$pro->id}}</h1>
    <form action="{{route('produits.update',['produit'=>$pro->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group mb-3">
        <label class="mb-1" for="designation">Designation</label>
        <input type="text" class="form-control" name="designation" id="designation" value="{{old('designation',$pro->designation)}}">
        </div>
        <div class="form-group mb-3">
        <label class="mb-1" for="prix_u">prix_u</label>
        <input type="text" class="form-control" name="prix_u" id="prix_u" value="{{old('prix_u',$pro->prix_u)}}">
        </div>
        <div class="form-group mb-3">
        <label class="mb-1" for="quantite_stock">quantite_stock</label>
        <input type="text" class="form-control" name="quantite_stock" id="quantite_stock" value="{{old('quantite_stock',$pro->quantite_stock)}}">
        </div>
        <div class="form-group mb-3">
        <label class="mb-1" for="categorie_id">Choose a categorie_id:</label>
        <select class="form-control" name="categorie_id" id="categorie_id">
        @foreach ($categories as $cat)

          <option value="{{ $cat->id }}" {{ ( $pro->categorie_id == $cat->id) ? 'selected' : '' }}>
                {{ $cat->id }}
            </option>

          @endforeach
        </select>

        </div>

        <div>
            <input class="btn btn-primary" type="submit" value="Modifier">
        </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>

           @endforeach
        </ul>



        @endif
    </div>
@endsection
