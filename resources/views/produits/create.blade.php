@extends('layouts.admin')
@section('title','Ajouter une produit')
@section('content')
    <h1>Créer une nouvelle produit</h1>
    <form  action="{{route('produits.store')}}" enctype="multipart/form-data" method="POST">
        @csrf
          <div class="form-group mb-3">
            <label class="mb-1" for="designation">Designation</label>
            <input type="text" class="form-control" name="designation" id="designation" value="{{old('designation')}}">
          </div>
          <div class="form-group mb-3">
            <label class="mb-1" for="prix_u">prix_u</label>
            <input type="text" class="form-control" name="prix_u" id="prix_u" value="{{old('prix_u')}}">
          </div>
          <div class="form-group mb-3">
            <label class="mb-1" for="quantite_stock">quantite_stock</label>
            <input type="text" class="form-control" name="quantite_stock" id="quantite_stock" value="{{old('quantite_stock')}}">
          </div>
          <div class="form-group mb-3">
            <label class="mb-1" for="categorie_id">Choose a categorie_id:</label>
            <select class="form-control" name="categorie_id" id="categorie_id">
              @foreach ($categories as $cat)
              <option value="{{$cat->id}}">{{$cat->id}}</option>
              @endforeach
            </select>
          </div>
            <div class="form-group mb-3">
              <label class="mb-1" for="image">Select Image</label>
              <input type="file" class="form-control" name="image" id="image">
            </div>

          <div>
              <input class="btn btn-primary" type="submit" value="Ajouter">
          </div>
    </form>
    <div>
        @if($errors->any())
        <h6>Errors: </h6>
        <ul>
          @foreach($errors->all() as $er)
          <li>{{$er}}</li>
          @endforeach
        </ul>



        @endif
    </div>
@endsection
