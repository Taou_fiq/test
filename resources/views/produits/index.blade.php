@extends('layouts.admin')
 @section('title','Gestion des produits')
 @section('content')

 <h1 class="my-2">Recherche de produit</h1>

 <form action="{{ route('produits.index') }}" method="GET">
     @csrf

        <div class="input-group m-3">
         <div class="input-group-prepend">
           <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Designation</span>
         </div>
         <input class="col-xs-5 border border-secondary  div-inputs" type="text" class="form-control" name="designation" id="designation" value="{{old('designation',$designation)}}" aria-describedby="basic-addon3">
        <div class="input-group-prepend ">
          <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Prix_min</span>
        </div>
        <input class="col-xs-5 border border-secondary " type="text" class="form-control" name="min_prix" id="designation" value="{{old('min_prix',$min_prix)}}" aria-describedby="basic-addon3">
        <div class="input-group-prepend ">
            <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Prix_max</span>
          </div>
          <input class="col-xs-5 border border-secondary div-inputs" type="text" class="form-control" name="max_prix" id="designation" value="{{old('max_prix',$max_prix)}}" aria-describedby="basic-addon3">
          <div class="input-group-prepend ">
            <div class="input-group-text">
              <input class="mx-2" type="checkbox" name="qnt_null" aria-label="Checkbox for following text input">
              <label for="checkbox" class="div-inputs" >Produits ayant un quantité nulle</label>
            </div>
          </div>
          <button class="btn btn-primary mx-3" type="submit">Rechercher</button>
      </div>
    </form>


    <h1 class="my-2">Liste des produits</h1>
    <a class="btn btn-primary m-2" href="{{route('produits.create')}}">Ajouter une nouvelle produit</a>

    <div class="custom-container ml-5 ">
      Résultats : <span class="badge bg-primary m-2"> {{$nbr_produit}}</span></div>
    <table class="table" id="tbl">
      <tr class="text-center">
        <th>Id</th>
        <th>Designation</th>
        <th>prix_u</th>
        <th>quantite_stock</th>
        <th>categorie_id</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($produits as $pro)
          <tr>
            <td class="text-center">{{$pro->id}}</td>
            <td>{{$pro->designation}}</td>
            <td class="text-center">{{$pro->prix_u}}</td>
            <td class="text-center">{{$pro->quantite_stock}}</td>
            <td class="text-center">{{$pro->categorie_id}}</td>
            <td class="text-center"><a class="btn btn-secondary" href="{{route('produits.show',['produit'=>$pro->id])}}">Details</a></td>
            <td class="text-center"><a class="btn btn-success" href="{{route('produits.edit',['produit'=>$pro->id])}}">Modifier</a></td>
            <td class="text-center">
                <form action="{{route('produits.destroy',['produit'=>$pro->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input class="btn btn-danger" type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette produit?')">
                </form></td>
          </tr>
      @endforeach
    </table>
    {{ $produits->links() }}
 @endsection
