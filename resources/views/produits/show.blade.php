@extends('layouts.admin')
 @section('title','Detail d \'une produit')
 @section('content')
    <a class="btn btn-primary" href="{{route('produits.index')}}">Retourner vers la liste des produits</a>
    <h1>Détail de la produit N° {{$pro->id}}</h1>
    <div>
        @if ($pro->image)

                <img src="{{ asset('storage/' . $pro->image) }}" alt="Product Image">
            @endif
        <p><strong>Designation:</strong> {{$pro->designation}}</p>
        <p><strong>prix_u:</strong> {{$pro->prix_u}}</p>
        <p><strong>quantite_stock:</strong> {{$pro->quantite_stock}}</p>
        <p><strong>categorie_id:</strong> {{$pro->categorie_id}}</p>
    </div>
@endsection
