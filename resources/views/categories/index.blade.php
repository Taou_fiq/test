 @extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')
    <h1 class="my-2">Liste des categories</h1>
    <a class="btn btn-primary m-2" href="{{route('categories.create')}}">Ajouter une nouvelle categorie</a>
    <a class="btn btn-primary m-2" href="{{route('categories.recherche')}}">Rechercher de categorie</a>
    <table class="table" id="tbl">
      <tr >
        <th class="text-center">Id</th>
        <th >Designation</th>
        <th class="text-center">Description</th>
        <th class="text-center" colspan="3">Actions</th>
      </tr>
      @foreach ($categories as $cat)
          <tr>
            <td class="text-center">{{$cat->id}}</td>
            <td>{{$cat->designation}}</td>
            <td>{{$cat->description}}</td>
            <td class="text-center"><a class="btn btn-secondary" href="{{route('categories.show',['category'=>$cat->id])}}">Details</a></td>
            <td class="text-center"><a class="btn btn-success" href="{{route('categories.edit',['category'=>$cat->id])}}">Modifier</a></td>
            <td class="text-center">
                <form action="{{route('categories.destroy',['category'=>$cat->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input class="btn btn-danger" type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette categorie?')">
                </form></td>
          </tr>
      @endforeach
    </table>
    {{ $categories->links() }}
 @endsection
