 @extends('layouts.admin')
 @section('title','Ajouter une categorie')
 @section('content')
    <h1>Créer une nouvelle categorie</h1>
    <form action="{{route('categories.store')}}" method="POST">
        @csrf
        <div class="form-group mb-3">
          <label class="mb-1"  for="designation">Designation</label>
          <input type="text" class="form-control name="designation" id="designation" value="{{old('designation')}}">
        </div>
         <div class="form-group mb-3">
        <label class="mb-1" for="description">Description</label>
       <textarea class="form-control mb-3" name="description" id="description"  rows="5" >{{old('designation')}}</textarea>

        </div>
        <div>
            <input class="btn btn-primary" type="submit" value="Ajouter">
        </div>
    </form>
    <div>
        @if($errors->any())
        <h6>Errors: </h6>
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>

           @endforeach
        </ul>
        @endif
    </div>
@endsection
