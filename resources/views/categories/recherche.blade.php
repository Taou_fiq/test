<!-- resources/views/categories/recherche.blade.php -->

@extends('layouts.admin') <!-- Make sure to adjust the layout as per your application -->

@section('title', 'Recherche de catégorie')

@section('content')

    <h1>Recherche de catégorie</h1>

    <form action="{{ route('categories.recherche') }}" method="GET">
        @csrf

        <label for="designation">Designation:</label>
        <input type="text" name="designation" id="designation" required>

        <button type="submit">Rechercher</button>
    </form>

    @if(isset($cat))
        <div>
            <h2>Résultat de la recherche</h2>
            <p>ID: {{ $cat->id }}</p>
            <p>Designation: {{ $cat->designation }}</p>
            <p>Description: {{ $cat->description }}</p>
        </div>
        
    @else
    <div> designation non trouver</div>
        
    @endif
    

@endsection
