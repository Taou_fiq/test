 @extends('layouts.admin')
 @section('title','Modifier une categorie')
 @section('content')
    <h1>Modifier la categorie N° {{$cat->id}}</h1>
    <form action="{{route('categories.update',['category'=>$cat->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group mb-3">
        <label class="mb-1" for="designation">Designation</label>
        <input type="text" class="form-control" name="designation" id="designation" value="{{old('designation',$cat->designation)}}">
        </div>
        <div class="form-group mb-3">
        <label class="mb-1" for="description">Description</label>
        <textarea name="description" class="form-control" id="description" cols="30" rows="5" >{{old('designation',$cat->description)}}</textarea>

        </div>
        <div>
            <input class="btn btn-primary" type="submit" value="Modifier">
        </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>

           @endforeach
        </ul>



        @endif
    </div>

@endsection
