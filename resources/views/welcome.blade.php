@extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')

 <div class="d-flex justify-content-evenly">
    <div class="card" style="width: 18rem;">
        <img src="{{ asset('/img/prduits.png')}}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Catégories</h5>
          <p class="card-text">vous pouvez consulter nos catégories en cliquant sur le button Consulter suivant</p>
          <a href="{{ route("categories.index") }}" class="btn btn-primary">Consulter</a>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img src=" {{ asset('/img/prduits.png')}}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Produits</h5>
          <p class="card-text">vous pouvez consulter nos produits en cliquant sur le button Consulter suivant </p>
          <a href="{{ route("produits.index") }}" class="btn btn-primary">Consulter</a>
        </div>
      </div>
 </div>

 @endsection
